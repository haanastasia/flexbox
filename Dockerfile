FROM node:latest

# Create app directory
RUN mkdir -p /var/www/html
WORKDIR /var/www/html

# Install app dependencies
COPY package.json /var/www/html
COPY webpack.config.js /var/www/html

RUN yarn

# Bundle app source
COPY src/ /var/www/html/src

# App setup
EXPOSE 30000

# App startup command
CMD ["npm", "run", "serve"]
