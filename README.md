# Fashionable #

New project using flexbox.

## Getting Started ##

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites ###
* node.js or docker

### Installing vie webpack ###
* Download project ( git clone https://haanastasia@bitbucket.org/haanastasia/flexbox.git )
* Run node.js
* npm run build
* npm run start

### Installing vie docker ###
* Download project ( git clone https://haanastasia@bitbucket.org/haanastasia/flexbox.git )
* Run docker
* docker build -t flexbox .
* docker run -p 30000:30000 flexbox
* open http://localhost:30000

## Built With ##
* SASS - Style sheets
* Flexbox -  Website layouts
* WOW.js & Animate.css - Animations when you scroll
* JS - Gallery

## Authors ##

* Anastasia Dolgopolova (https://bitbucket.org/haanastasia/flexbox)