Array.prototype.forEach.call(document.querySelectorAll('.gallery__image-min'), function (el) {
    el.addEventListener('click', function (e) {
        var btnClicked = e.currentTarget.getAttribute('src');
        gallery(btnClicked);
    });
});


function gallery(img) {
    document.getElementById('gallery__image-big').classList.toggle('gallery__image-big--one');
    document.getElementById('gallery__image-big').classList.toggle('gallery__image-big--two');
    document.getElementById('gallery__image-big').src = img;
}