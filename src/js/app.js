import WOW from 'wowjs';

new WOW.WOW().init();

import './gallery/gallery.js';
import 'animate.css';
import '../sass/styles.scss';

function importAll(r) {
    return r.keys().map(r);
}

const images = importAll(require.context('../userfiles/', true, /\.(png|jpe?g|svg)$/));

const icon = importAll(require.context('../../assets/icon/', false, /\.(png|jpe?g|svg)$/));